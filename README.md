#MutableDict

MutibleDict extends the functionality of python-3.7's native dict() class.
It inherits all the behavior of dict-3.7 but adds a insert_before and
insert_after methods.

In python-3.7 dictionary's maintain their insertion order. This can be kind of nice but you have no 
ability to control the order after creation. 

This allows you to maintain the order that you need, if you need.

##Usage:

```Python

## import string to populate our MutableDict
import string
## import our MutableDict
from MutableDict import MutableDict as MD

## create our MutableDict with arbitrary values
md = MD({x: ord(x) for x in string.ascii_lowercase[:10]})

## print our MutableDict
print(md)

# Output: 
# {'a': 97, 'b': 98, 'c': 99, 'd': 100, 'e': 101, 'f': 102, 'g': 103, 'h': 104, 'i': 105, 'j': 106}

## Insert a random key and value after the "i" key
md.insert_after("i", "zzrr", ord("@"))

## print our MutableDict to see our new order
print(md)

# Output: 
# {'a': 97, 'b': 98, 'c': 99, 'd': 100, 'e': 101, 'f': 102, 'g': 103, 'h': 104, 'i': 105, 'zzrr': 64, 'j': 106}

## Insert a pseudo-random key and value before the "d" key
md.insert_before("d", "superFunktasticMyDudes", ord("&"))

## print our MutableDict to see our new order
print(md)

# Output: 
# {'a': 97, 'b': 98, 'c': 99, 'superFunktasticMyDudes': 38, 'd': 100, 'e': 101, 'f': 102, 'g': 103, 'h': 104, 'i': 105, 'zzrr': 64, 'j': 106}

```

## Speed tests

```Python

from itmeit import timeit

In : timeit('MD({x: ord(x) for x in string.ascii_lowercase})', setup='import string; from MutableDict import MutableDict as MD', number=1000000)
Out: 2.494404876997578

In : timeit('v.insert_after("t", "zzrr", ord("z"))', setup='import string; from MutableDict import MutableDict as MD; v = MD({x: ord(x) for x in string.ascii_lowercase})' ,number=1000000)                                          
Out: 3.8934062769985758

```
